<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());


$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});
$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});
$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);   
});

$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload=[];
   foreach($messages as $msg){
    $payload[$msg->id] = [
        'body'=>$msg->body,
        'user_id'=>$msg->user_id,
        'created_at'=>$msg->created_at];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/messages', function($request, $response,$args){
   $message = $request->getParsedBodyParam('message','');
   $userid = $request->getParsedBodyParam('userid','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id = $userid;//all messages arriving are from user id 1
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
   } else {
        return $response->withStatus(400);
   }
});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $_message = Message::find($args['message_id']);
    $_message->delete();
    if($_message->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
    $email = $request->getParsedBodyParam('email','');
    
   $_user = new User();
   $_user->username = $username;
   $_user->email = $email;
   $_user->save();
   if($_user->id){
       $payload = ['user_id'=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/users/{id}', function($request, $response,$args){
    $_user = User::find($args['id']);
    $_user->delete();
    if($_user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});




//lesson 3 rony
$app->put('/messages/{message_id}', function($request, $response,$args){
    $new_message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    $_message->body = $new_message;
     if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
     } else {
        return $response->withStatus(400);
     }
});

$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();//מה שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך 
    Message::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});



$app->run();
